## :hatched-chick: Pokedex :octopus:

There is a million-and-one pokedex out there in the repository space, why rinse and repeat? 

Because the api is straight forward to focus on revising hooks, to understand passing props, pagination, and attempt at favouriting.

## :gift: NPM Packages

* Bootstrap
* styled-components
* axios for making api calls

## :thought_balloon: Thoughts

- Import bootstrap directly into `app.js`

```javascript
import 'bootstrap/dist/css/bootstrap.min.css'
```

State of component is set and changd by UI changes. Passed through components. 
State is always an object.

*To pass props into a lower level component*
1. set up props in the parent component first 
> <PokemonList propname={apiresult}/>

2. in destination-child component, initialise state with base default values ABOVE THE RENDER

```javascript
  state={
    name: "",
    imageURL: "",
    pokemonIndex: ""
  }
```

**You don't need to do this if you destructure.**

3. in destination-child component, feed in props as argument for the render method

```javascript
render(props) {
  return (
    // display html things in here
  )
}
```

4. in destination-child component,  draw out the 'properties' of the props by initialising separate variables INSIDE THE RENDER BUT OUTSIDE OF THE RETURN.

```javascript
    const name = this.props.name
    const url = this.props.imageURL
    const index = this.props.pokemonIndex
```

5. Pass in the variables in the return statement as html
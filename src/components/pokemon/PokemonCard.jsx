import React, { Component } from 'react'
import styled from 'styled-components'
import spinner from "../pokemon/spinner.gif"
// const Sprite = styled.img`
//   width: 5em;
//   height: 5em;
//   display: block;
// `


const Card = styled.div`
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
  &:hover{
    box-shadow: 0 14px 29px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  }
  user-select:none;
`

export default class PokemonCard extends Component {

  // need to intialise state
  state={
    name: "",
    imageURL: "",
    pokemonIndex: "",
    imageLoading: true,
    tooManyRequest: false
  }

  componentDidMount(){
    const {name, url} = this.props
    // getting the API index of the pokemon
    const pokemonIndex = url.split("/")[url.split("/").length - 2]
    const imageURL = `https://github.com/PokeAPI/sprites/blob/master/sprites/pokemon/${pokemonIndex}.png?raw=true`

    this.setState({name, imageURL, pokemonIndex})
  }

  render() {
    // create separate variables for each property of the props being passed in
    // const name = this.props.name
    // const url = this.props.imageURL
    // const index = this.props.pokemonIndex

    // same as above
    // const {name, url, index} = this.props 

    return (
      <div className="col-md-3 col-sm-6 mb-5">
        <Card className="card">
          <h6 className="card-header">#{this.state.pokemonIndex}</h6>
          {this.state.imageURL !== "" ? (
            <img src={this.state.imageURL} className="mx-auto" style={{width: "80%", height: "80%"}} alt={this.state.pokemonIndex}/>
          ) : (
            // also not working here
            <img src={spinner} className="mx-auto" style={{width: "100%", height: "100%"}}/>
          )}
            {/* Not working.Needing onLoad and onError to get going <Sprite
              className="card-img-top rounded mx-auto mt-2"
              src={this.state.imageUrl}/> */}
          <div className="mx-auto card-body p-4">
            <h6 className="card-title">{this.state.name.charAt(0).toUpperCase() + this.state.name.substring(1)}</h6>
          </div>
        </Card>
      </div>
    )
  }
}

import React, { Component, Fragment} from 'react'
import PokemonCard from "./PokemonCard"
import axios from 'axios'

export default class PokemonList extends Component {

  state = {
    url: `https://pokeapi.co/api/v2/pokemon/`,
    pokemon: null // data stored from the api call
  }

  async componentDidMount(){
    const res = await axios.get(this.state.url)
    console.log("there is data", res)
    this.setState({pokemon: res.data.results})  
    console.log("there is pokemon", this.state.pokemon)
  }
  
  render() {
    return (
      <Fragment>
        {this.state.pokemon ? (
          <div className="row">
          {this.state.pokemon.map((pokemon, idx) => (
            <PokemonCard name={pokemon.name} url={pokemon.url} key={idx}/>
          ))}
          </div>
        ) : (
          <h1>Loading Pokemon now ... </h1>
        )}
      </Fragment>
      // Successful drew data out
      // <div className="row">
      //   {console.log("hello", this.state.pokemon)}
      //   {this.state.pokemon != null? (this.state.pokemon.map((item, index) => (
      //     <p key={index}>{item.name}</p>
      //   ))) : (<h1>Loading</h1>)}
      // </div>
    )
  }
}
